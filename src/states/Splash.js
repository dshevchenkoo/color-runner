import Phaser from 'phaser'
import { centerGameObjects } from '../utils'

export default class extends Phaser.State {
  init () {}

  preload () {
    this.loaderBg = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loaderBg')
    this.loaderBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loaderBar')
    centerGameObjects([this.loaderBg, this.loaderBar])

    this.load.setPreloadSprite(this.loaderBar)
        
    //
    // load your assets
    //
    this.load.audio('audio_game', ['assets/audio/colorunner.mp3']);
    this.load.audio('audio_menu', ['assets/audio/colorunner_menu.mp3']);
    this.load.audio('trombone', ['assets/audio/trombone.mp3']);
    this.load.image('colorrunner_gameover', 'assets/images/colorrunner_gameover.png');
    this.load.image('game_opening', 'assets/images/resource.png')
    this.load.image('sky', 'assets/images/back1.png', window.innerWidth, window.innerHeight)
    this.load.image('green', 'assets/images/game_green-04.svg')
    this.load.image('red', 'assets/images/game_red.svg')
    this.load.image('yellow', 'assets/images/game_yellow.svg')
    this.load.image('progress1', 'assets/images/progress_1.png')
    this.load.image('progress2', 'assets/images/progress_2.png')
    this.load.image('parachute', 'assets/images/parash.png')
    this.load.spritesheet('dude', 'assets/images/colorrunner_sprite_orange_new.png', 73, 100)
    this.load.spritesheet('enemy', 'assets/images/colorrunner_sprite_green_new.png', 73, 100)
  }

  create () {
    this.state.start('Start')
  }
}