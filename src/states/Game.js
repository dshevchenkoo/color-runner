/* globals __DEV__ */
import Phaser from 'phaser'
import ColorBlock from '../sprites/ColorBlock'

export default class extends Phaser.State {

  init () {}
  preload () {}

  create () {
    this.audio = this.add.audio('audio_game').loopFull()
    this.game.world.setBounds(0, 0, 8000, 1.2 * this.game.height)
    this.game.physics.startSystem(Phaser.Physics.ARCADE)
    let sky = this.game.add.tileSprite(0, 0, 8000, 1.2 * this.game.height, 'sky')

    this.blocks = this.game.add.group()
    this.blocks.enableBody = true

    this.enemies = this.game.add.group()

    this.addGroundBlocks()

    this.add.text(0.69 * this.game.width, 65, 'Speed',
      { font: '45px Arial', fill: '#ffffff', align: 'center' }).fixedToCamera = true
    this.add.text(0.7 * this.game.width, 165, 'Jump',
      { font: '45px Arial', fill: '#ffffff', align: 'center' }).fixedToCamera = true
    this.score = 0;
    this.scoreText = this.game.add.text(0.7 * this.game.width, 265,'Score: ' + this.score,
      { font: '45px Arial', fill: '#ffffff', align: 'center' })
    this.scoreText.fixedToCamera = true

    this.player = this.game.add.sprite(100, this.world.height / 1.8, 'dude')

    //  We need to enable physics on the player
    this.game.physics.arcade.enable(this.player)

    //  Player physics properties. Give the little guy a slight bounce.
    this.player.body.bounce.y = 0
    this.player.body.gravity.y = 500
    this.player.body.collideWorldBounds = true

    //  Our two animations, walking left and right.
    this.player.animations.add('left', [2, 1, 2, 1], 10, true)
    this.player.animations.add('right', [1, 3, 1, 3], 10, true)

    //  Move to the right
    this.player.body.velocity.x = 450
    //  Default jump height
    this.player.body.jumpHeight = -350

    //Max values of speed and jump
    this.player.body.maxSpeed = 600
    this.player.body.maxJump = -400

    //add progressbar
    this.add.sprite(0.8 * this.game.width, 70, 'progress1').fixedToCamera = true
    this.add.sprite(0.8 * this.game.width, 170, 'progress1').fixedToCamera = true

    this.speedProgressBar = this.game.add.sprite(0.8 * this.game.width, 70, 'progress2')
    this.jumpProgressBar = this.game.add.sprite(0.8 * this.game.width, 170, 'progress2')
    this.speedProgressBar.fixedToCamera = true
    this.jumpProgressBar.fixedToCamera = true

    this.updateProgressBar()

    this.timer = this.game.time.create(false)
    this.timer.loop(1000, this.createNewEnemy, this)
    this.timer.start()

    this.game.camera.follow(this.player)
  }

  update () {

    let cursors = this.game.input.keyboard.createCursorKeys()
    let player = this.player

    this.updateEnemyVelocity()
    this.updateEnemyState()

    //  Collide the player and the enemies with the platforms
    let hitPlatform = this.game.physics.arcade.collide(this.player, this.blocks, this.updatePlayerState, null, this)
    let hitEnemy = this.game.physics.arcade.collide(this.player, this.enemies)
    this.game.physics.arcade.collide(this.enemies, this.blocks)

    if (hitEnemy) {
      localStorage.setItem('score', this.score)
      if (localStorage.getItem('highscore') === null) {
        localStorage.setItem('highscore', this.score)
      }
      else if (this.score > localStorage.getItem('highscore')) {
        localStorage.setItem('highscore', this.score)
      }
      this.state.start('GameOver')
      this.audio.stop();
    }

    if (player.x >= this.world.width - 2.0 * player.width) {
      this.enemies.destroy()
      this.blocks.destroy()
      this.blocks = this.game.add.group()
      this.enemies = this.game.add.group()
      this.addGroundBlocks()
      player.x = 0
      this.score++
      this.scoreText.setText('Score: ' + this.score)
    }

    this.player.animations.play('right')

    if (cursors.up.isDown && player.body.touching.down && hitPlatform) {
      player.body.velocity.y = player.body.jumpHeight
    }

    this.updateProgressBar()
  }

  generateBlockColour () {
    let colours = ['red', 'green', 'yellow']
    return colours[Math.round(Math.random() * (colours.length - 1))]
  }

  generateBlockWidth () {
    let width = Math.random() * 0.1 * this.game.width
    return width < 150 ? 150 : width
  }

  addGroundBlocks () {
    let blockGroupWidth = 0
    while (blockGroupWidth <= this.world.width + 1000) {
      let blockWidth = this.generateBlockWidth()
      let block = new ColorBlock({
        game: this.game,
        x: blockGroupWidth,
        y: this.world.height + 100,
        asset: this.generateBlockColour(),
        width: blockWidth
      })
      this.game.physics.arcade.enable(block)
      block.body.immovable = true
      this.blocks.add(block)
      blockGroupWidth += blockWidth
    }
  }

  updateEnemyState () {
    for (let i = 0; i < this.enemies.children.length; ++i) {
      if (this.enemies.children[i].y > this.game.height / 1.8 && this.enemies.children[i].parachute === true) {
        this.enemies.children[i].loadTexture('enemy', 4, false)
        this.enemies.children[i].animations.add('right', [1, 1, 3, 3], 10, true)
        this.enemies.children[i].animations.add('left', [6, 6, 7, 7], 10, true)
        this.enemies.children[i].animations.play('right', 30, true)
        this.enemies.children[i].parachute = false
      }
    }
  }

  updatePlayerState (player, block) {
    switch(block.colour) {
      case "green":
        if (player.body.velocity.x < player.body.maxSpeed) {
          player.body.velocity.x += 5
        }
        if (player.body.jumpHeight > player.body.maxJump) {
          player.body.jumpHeight -= 5
        }
        break
      case "red":
        if (player.body.velocity.x > 150) {
          player.body.velocity.x -= 50
        }
        break
      case "yellow":
        if (this.player.body.jumpHeight < -120) {
          this.player.body.jumpHeight += 50
        }
        break;
    }
  }

  updateEnemyVelocity () {
    for (let i = 0; i < this.enemies.children.length; ++i) {
      this.enemies.children[i].body.velocity.x = 0
      if (Math.abs(this.player.x - this.enemies.children[i].x) > 30 &&
        Math.abs(this.player.x - this.enemies.children[i].x) < 1000) {
        if (this.player.x < this.enemies.children[i].x) {
          this.enemies.children[i].x = -450
          this.enemies.children[i].animations.play('left')
        }
        else {
          this.enemies.children[i].body.velocity.x = 450
          this.enemies.children[i].animations.play('right')
        }
      }
    }
  }

  updateProgressBar () {
    this.speedProgressBar.scale.x = this.player.body.velocity.x / this.player.body.maxSpeed
    this.jumpProgressBar.scale.x = this.player.body.jumpHeight / this.player.body.maxJump
  }

  createNewEnemy() {
    let enemy = this.game.add.sprite(this.game.camera.x + game.rnd.integerInRange(200, 900), 0, 'parachute')
    this.game.physics.arcade.enable(enemy)
    enemy.body.gravity.y = 500
    enemy.body.collideWorldBounds = true
    enemy.parachute = true
    this.enemies.add(enemy)
  }

  render () { }
}