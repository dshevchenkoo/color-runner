import Phaser from 'phaser'

export default class extends Phaser.Sprite {
  constructor ({ game, x, y, asset, width}) {
    super(game, x, y, asset)
    this.anchor.setTo(1)
    this.colour = asset
    this.width = width
    this.scale.x = 1.1
  }

  update () {

  }
}